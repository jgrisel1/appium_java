import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.AppiumFluentWait;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.touch.WaitOptions;

public class TestClass{
	
	private AndroidDriver<AndroidElement> driver;
	
	@Before
	public void setUp() throws MalformedURLException {
	//paramètre de l'environnement
	DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
	desiredCapabilities.setCapability("platformName", "Android");
	desiredCapabilities.setCapability("platformVersion", "8.1");
	desiredCapabilities.setCapability("deviceName", "TOTO");
	desiredCapabilities.setCapability("appPackage", "com.simplemobiletools.contacts");
	desiredCapabilities.setCapability("appActivity", ".activities.MainActivity t11");
	//URL DEVICE
	URL remoteUrl = new URL("http://localhost:4723/wd/hub");
	//instanciation du driver
	driver = new AndroidDriver<AndroidElement>(remoteUrl, desiredCapabilities);
	//implicit Wait
	driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}
	
	@Test
	public void Simplecontact() throws InterruptedException {
		MobileElement el1 = (MobileElement) driver.findElementById("com.android.packageinstaller:id/permission_allow_button");
		el1.click();
		WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(MobileBy.id("com.android.packageinstaller:id/permission_allow_button"))).click();
		MobileElement el3 = (MobileElement) driver.findElementById("com.simplemobiletools.contacts:id/fragment_fab");
		el3.click();
		MobileElement el4 = (MobileElement) driver.findElementById("com.simplemobiletools.contacts:id/contact_first_name");
		el4.sendKeys("Jules");
		MobileElement el5 = (MobileElement) driver.findElementById("com.simplemobiletools.contacts:id/contact_surname");
		el5.sendKeys("gr");
		MobileElement el6 = (MobileElement) driver.findElementById("com.simplemobiletools.contacts:id/contact_number");
		el6.sendKeys("0687748578");
		MobileElement el7 = (MobileElement) driver.findElementById("com.simplemobiletools.contacts:id/contact_numbers_add_new");
		el7.click();
		driver.hideKeyboard();
		MobileElement el8 = (MobileElement) driver.findElementById("com.simplemobiletools.contacts:id/contact_email");
		el8.sendKeys("gr@gmail.com");
		MobileElement el9 = (MobileElement) driver.findElementByAccessibilityId("Save");
		el9.click();
		MobileElement el10 = (MobileElement) driver.findElementById("com.simplemobiletools.contacts:id/contact_name");
		el10.click();
	}
	
	@After
	public void teardown() {
	driver.quit();
	}
}