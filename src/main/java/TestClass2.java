import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class TestClass2{
	
	private AndroidDriver<AndroidElement> driver;
	
	@Before
	public void setUp() throws MalformedURLException {
	//paramètre de l'environnement
	DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
	desiredCapabilities.setCapability("platformName", "Android");
	desiredCapabilities.setCapability("platformVersion", "8.1");
	desiredCapabilities.setCapability("deviceName", "TOTO");
	desiredCapabilities.setCapability("appPackage", "com.android.calculator2");
	desiredCapabilities.setCapability("appActivity", ".Calculator");
	//URL DEVICE
	URL remoteUrl = new URL("http://localhost:4723/wd/hub");
	//instanciation du driver
	driver = new AndroidDriver<AndroidElement>(remoteUrl, desiredCapabilities);
	//implicit Wait
	driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}
	
	@Test
	public void Calculator() {
		MobileElement el1 = (MobileElement) driver.findElementById("com.android.calculator2:id/digit_4");
		el1.click();
		MobileElement el2 = (MobileElement) driver.findElementByAccessibilityId("multiply");
		el2.click();
		MobileElement el3 = (MobileElement) driver.findElementById("com.android.calculator2:id/digit_3");
		el3.click();	
	}
}